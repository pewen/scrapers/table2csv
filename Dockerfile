FROM node:12.10.0-alpine

# make the 'app' folder the current working directory
WORKDIR /app

EXPOSE 3000
CMD [ "yarn", "run", "dev" ]
