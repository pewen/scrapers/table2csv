# Scrapear

> Web data scraper tool kit

**Scraperar** es una serie de herramientas para operaciones comunes de extraccion de datos de la web.
Estas herramientas pueden ser utilizadas mediente la interfaz web o programaticamente usando nuestro API.

Todas las herramientas desarrolladas son libres y gratuitas y toda contrubucion es mas que bienvenida.

## Table of Contents

- [Run with docker](#run-with-docker-compose)
- [Herramientas](#herramientas)
  - [HTML table to CSV](#table2csv)
  - [Maps extractor](#map-extractor)
- [TODO](#todo)
  - [Frontend](#frontend)
  - [Backend](#backend)

## Run with docker

Tenes que tener instalado docker y docker-compose en tu sistema.

```bash
$ docker-compose build
$ docker-compose run --rm web yarn install
$ docker-compose up
```

## Herramientas

Listado de herramientas y como usarlas

### Table2CSV

HTML table to CSV.  
Extracting HTML tables

### Map extractor

## TODO

### Frontend

- [x] Poder paginar sobre un url, por ej *https://rdlist.nic.ar/rd_list/?page=2*
- [x] Barra de progreso en el caso de muchas paginas
- [x] Paginacion sobre la tabla obtenida
- [ ] Si se obtiene de muchas paginas, agregar una columna de pagina a la tabla
- [ ] Si la tabla tiene comas rompe el csv
- [ ] Mejorar la estetica del sitio
- [x] Meter el front en el docker-compose
- [ ] Docker-compose para production
- [ ] Spinner cuando se este cargando el resultado del server

### Backend

- [ ] Si el url no es valido, rompe el back
- [ ] Hacer el get async
