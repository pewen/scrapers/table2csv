import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { faTwitter, faGitlab } from '@fortawesome/free-brands-svg-icons'

// Unpack some font awesome icons
library.add(faTwitter, faGitlab)
Vue.component('font-awesome-icon', FontAwesomeIcon)
