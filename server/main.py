import time
from string import ascii_lowercase

import requests
import pandas as pd
from bs4 import BeautifulSoup
from fastapi import FastAPI, HTTPException
from starlette.middleware.cors import CORSMiddleware


def scrap (url):
    """

    """
    start = time.time()

    response = requests.get(url)
    soup = BeautifulSoup(response.text, features="lxml")
    tables = soup.find_all('table')

    #
    data = []

    for column in tables[0].find_all('tr'):
        raw_line = [[' '.join(ele.text.split()), ele.find('a'),
                    ele.find('img')] for ele in column.find_all('td')]

        # Flatten the list
        line = [ele for partial in raw_line for ele in partial]
        data.append(line)

    df = pd.DataFrame(data)

    # Extraigo el link
    for c in df.columns[1: -1: 3]:
        df[c] = df[c].apply(lambda ele: ele.attrs['href'] if ele else None)

    # Extraigo el src de la img
    for c in df.columns[2: -1: 3]:
        df[c] = df[c].apply(lambda ele: ele.attrs['src'] if ele else None)

    # Get the columns names
    if tables[0].find('thead'):
        header = [ele.text for ele in tables[0].find('thead').find_all('th')]
    else:
        header = ascii_lowercase

    # Agrego el base_url a los links
    base_url = '/'.join(url.split('/')[:3])
    for c in df.columns[1: -1: 3]:
        df[c] = df[c].apply(lambda ele: base_url + ele if ele and ele.startswith('/') else ele)

    for c in df.columns[2: -1: 3]:
        df[c] = df[c].apply(lambda ele: base_url + ele if ele and ele.startswith('/') else ele)


    num_cols = int(df.columns.shape[0] / 3)
    columns = [[header[i], header[i] + '_link', header[i] + '_img']
                for i in range(num_cols)]
    columns = [ele.lower() for partial in columns for ele in partial]

    df.columns = columns
    df = df.dropna(how='all', axis=0)
    df = df.dropna(how='all', axis=1)

    total = time.time() - start

    return df, total


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/table2csv/")
def get_data(url: str = ''):
    """

    """
    if not url:
        raise HTTPException(status_code=404, detail="URL not specified")

    df, total = scrap(url)
    # df_split = df.to_dict(orient='split')

    return {
        "time": total,
        "columns": df.columns.tolist(),
        'data': df.to_dict(orient='records')
    }
